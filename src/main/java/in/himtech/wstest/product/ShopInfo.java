package in.himtech.wstest.product;

import in.himtech.wstest.product.exception.InvalidInputException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService(name="TestMartShopInfor", targetNamespace="http://wstest.himtech.in")
@SOAPBinding(style=Style.RPC)
public class ShopInfo {

	@WebMethod(action="fetch_shopInfo", operationName="fetchShopInfo")
	@WebResult(partName="outputShopInfo", name="helloShopResult")
	public String getShopInformation(@WebParam(partName="inputShopInfo")String condition) throws InvalidInputException{
		String result = null;
		
		if(condition.equals("name")){
			result = "Himpc ki dukaan";
		} else if(condition.equals("since")){
			result = "1883";
		} else {
			throw new InvalidInputException("Invalid Input", condition + "is not a valid input");
		}
		
		return result;
	}
}
