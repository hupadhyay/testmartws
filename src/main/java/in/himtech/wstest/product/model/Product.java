package in.himtech.wstest.product.model;

public class Product {
	private String name;
	private int ssn;
	private double cost;

	public Product(String string, int i, double d) {
		name = string;
		ssn = i;
		cost = d;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

}
