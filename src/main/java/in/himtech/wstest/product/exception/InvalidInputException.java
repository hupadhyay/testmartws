package in.himtech.wstest.product.exception;

public class InvalidInputException extends Exception {
	private String faultInfo;
	
	public InvalidInputException(String msg, String fInfo) {
		super(msg);
		this.faultInfo = fInfo;
	}
	
	public String getFaultInfo() {
		return faultInfo;
	}
}
