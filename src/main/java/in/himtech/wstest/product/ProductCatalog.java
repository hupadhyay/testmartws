package in.himtech.wstest.product;

import in.himtech.wstest.product.model.Product;
import in.himtech.wstest.product.services.ProductService;
import in.himtech.wstest.product.services.ProductServiceImpl;

import java.util.List;

import javax.jws.WebService;

@WebService(endpointInterface="in.himtech.wstest.product.ProductCalalogInterface")
public class ProductCatalog implements ProductCalalogInterface {
	private ProductService productService = new ProductServiceImpl();

	/* (non-Javadoc)
	 * @see in.himtech.wstest.product.ProductCalalogInterface#getProductCatagories()
	 */
	@Override
	public List<String> getProductCatagories() {
		return productService.getProductCatagories();
	}

	/* (non-Javadoc)
	 * @see in.himtech.wstest.product.ProductCalalogInterface#getProducts(java.lang.String)
	 */
	@Override
	public List<String> getProducts(String strCat) {
		return productService.getProducts(strCat);
	}

	/* (non-Javadoc)
	 * @see in.himtech.wstest.product.ProductCalalogInterface#addProduct(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean addProduct(String catagory, String value) {
		boolean bool = productService.addProduct(catagory, value);
		return bool;
	}
	
	/* (non-Javadoc)
	 * @see in.himtech.wstest.product.ProductCalalogInterface#getProductObject(java.lang.String)
	 */
	@Override
	public List<Product> getProductObject(String strCat) {
		return productService.getProductsObject(strCat);
	}
}
