package in.himtech.wstest.product.services;

import in.himtech.wstest.product.model.Product;

import java.util.List;

public interface ProductService {

	public abstract List<String> getProducts(String strCat);

	public abstract boolean addProduct(String category, String value);

	public abstract List<String> getProductCatagories();

	public abstract List<Product> getProductsObject(String strCat);

}