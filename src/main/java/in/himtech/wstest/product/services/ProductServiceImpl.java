package in.himtech.wstest.product.services;

import in.himtech.wstest.product.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {

	List<String> listBooks;
	List<String> listMusics;
	List<String> listMovies;

	public ProductServiceImpl() {
		listBooks = new ArrayList<String>();
		listMusics = new ArrayList<String>();
		listMovies = new ArrayList<String>();

		listBooks.add("Learn WebServices");
		listBooks.add("Learn Hibernate");
		listBooks.add("Learn SpringMVC");
		listBooks.add("Learn CoreJava");

		listMusics.add("Lata");
		listMusics.add("Mukesh");
		listMusics.add("Kishore");
		listMusics.add("Rafi");
		listMusics.add("Sonu Nigam");

		listMovies.add("Alone");
		listMovies.add("Hawaijade");
		listMovies.add("Mardaani");
		listMovies.add("Tevar");
		listMovies.add("Linga");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * in.himtech.wstest.product.services.ProductService#getProducts(java.lang
	 * .String)
	 */
	@Override
	public List<String> getProducts(String strCat) {
		List<String> listProduct;

		if (strCat.equals("books")) {
			listProduct = listBooks;
		} else if (strCat.equals("musics")) {
			listProduct = listMusics;
		} else {
			listProduct = listMovies;
		}

		return listProduct;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * in.himtech.wstest.product.services.ProductService#addProduct(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public boolean addProduct(String category, String value) {
		boolean bool = false;
		System.out.println(category.toLowerCase());

		if (category.equals("books")) {
			bool = listBooks.add(value);
		} else if (category.equals("musics")) {
			bool = listMusics.add(value);
		} else if (category.equals("movies")) {
			bool = listMovies.add(value);
		} else {
			bool = false;
		}

		return bool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * in.himtech.wstest.product.services.ProductService#getProductCatagories()
	 */
	@Override
	public List<String> getProductCatagories() {
		List<String> listProductCategory = new ArrayList<String>();
		listProductCategory.add("Books");
		listProductCategory.add("Musics");
		listProductCategory.add("Moives");
		return listProductCategory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * in.himtech.wstest.product.services.ProductService#getProductsObject(java
	 * .lang.String)
	 */
	@Override
	public List<Product> getProductsObject(String strCat) {
		List<Product> listProduct = new ArrayList<Product>();
		listProduct.add(new Product("AALU", 123, 25.50));
		listProduct.add(new Product("GOBHI", 341, 20.50));
		listProduct.add(new Product("LAUKI", 321, 45.50));
		return listProduct;
	}
}
