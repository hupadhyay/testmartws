package in.himtech.wstest.product;

import in.himtech.wstest.product.model.Product;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(name = "TestMartCatalog", portName = "TestMartCatalogPort", 
serviceName = "TestMartCatalogService", targetNamespace="http://himmart.co.in")
public interface ProductCalalogInterface {

	@WebMethod(action="fetch_category", operationName="fetchCategories")
	public abstract List<String> getProductCatagories();

	@WebMethod(action="fetch_products", operationName="fetchProducts")
	public abstract List<String> getProducts(String strCat);

	@WebMethod(action="add_product", operationName="addProduct")
	public abstract boolean addProduct(String catagory, String value);

	@WebMethod(action="fetch_product_objects", operationName="fetchProductsObject")
	public abstract List<Product> getProductObject(String strCat);

}