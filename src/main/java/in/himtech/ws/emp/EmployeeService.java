package in.himtech.ws.emp;

import java.util.List;

public interface EmployeeService {
	Employee getEmployee(String id);

	List<Employee> listEmployee();

	void addEmployee(Employee emp);

	boolean updateEmployee(Employee emp);

	boolean deleteEmployee(String id);
}
