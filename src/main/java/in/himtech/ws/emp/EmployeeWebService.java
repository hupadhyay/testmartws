package in.himtech.ws.emp;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class EmployeeWebService {
	private EmployeeService employeeService;
	
	public EmployeeWebService() {
		employeeService = new EmployeeImplementation();
	}
	
	@WebMethod
	public Employee getEmployee(String empID){
		return employeeService.getEmployee(empID);
	}
	
	@WebMethod
	public List<Employee> getEmployeeList(){
		return employeeService.listEmployee();
	}
	
	@WebMethod
	public String updateEmployeeName(String empID, String Name){
		Employee emp = employeeService.getEmployee(empID);
		emp.setName(Name);
		boolean bool = employeeService.updateEmployee(emp);
		if(bool){
			return "Employee name updated successfully.";
		} else {
			return "Internal error occur";
		}
	}
	
	@WebMethod
	public String updateEmployeeSalary(String empID, double sal){
		Employee emp = employeeService.getEmployee(empID);
		emp.setSalary(sal);
		boolean bool = employeeService.updateEmployee(emp);
		if(bool){
			return "Employee salary updated successfully.";
		} else {
			return "Internal error occur";
		}
	}
	
	@WebMethod
	public String deleteEmployee(String empId){
		boolean bool = employeeService.deleteEmployee(empId);
		if(bool){
			return "Employee deleted successfully.";
		} else {
			return "Employee can be deleted";
		}
	}
}
