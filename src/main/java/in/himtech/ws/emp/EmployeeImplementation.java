package in.himtech.ws.emp;

import java.util.ArrayList;
import java.util.List;

public class EmployeeImplementation implements EmployeeService{
	private List<Employee> listEmployee;
	
	public EmployeeImplementation() {
		listEmployee  = new ArrayList<>();
		
		listEmployee.add(new Employee("E001", "Ramesh", 12, 2100));
		listEmployee.add(new Employee("E002", "Mukesh", 15, 2400));
		listEmployee.add(new Employee("E003", "Suresh", 18, 2700));
	}

	@Override
	public Employee getEmployee(String id) {
		Employee employee = null;
		for(Employee emp : listEmployee){
			if(id.equals(emp.getEmpId())){
				employee = emp;
				break;
			}
		}
		return employee;
	}

	@Override
	public List<Employee> listEmployee() {
		return listEmployee;
	}

	@Override
	public void addEmployee(Employee emp) {
		if(emp != null){
			listEmployee.add(emp);
		}
	}

	@Override
	public boolean updateEmployee(Employee pEmp) {
		boolean bool = false;
		for(Employee emp : listEmployee){
			if(pEmp.getEmpId().equals(emp.getEmpId())){
				listEmployee.remove(emp);
				listEmployee.add(pEmp);
				bool = true;
				break;
			}
		}
		return bool;
	}

	@Override
	public boolean deleteEmployee(String id) {
		boolean bool = false;
		for(Employee emp : listEmployee){
			if(id.equals(emp.getEmpId())){
				listEmployee.remove(emp);
				bool = true;
				break;
			}
		}
		return bool;
	}

}
